package com.chawico.showcase.bootlog4j2.usecase.gateway;

import java.util.Optional;
import com.chawico.showcase.bootlog4j2.usecase.domain.PersonDomain;

/**
 * Gateway responsible for retrieving a Person.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 29/04/2019
 */
public interface SearchPersonGateway {

  /**
   * Finds a person by its identifier.
   * 
   * @param personId {@code Long} - person identifier.
   * @return a person if found or empty if not.
   */
  Optional<PersonDomain> findPersonById(Long personId);
}
