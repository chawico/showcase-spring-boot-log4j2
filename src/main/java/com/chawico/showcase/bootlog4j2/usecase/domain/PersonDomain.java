package com.chawico.showcase.bootlog4j2.usecase.domain;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Representation of a Person according to the domain needs.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 29/04/2019
 */
@Getter
@Setter
@Builder
@ToString
public class PersonDomain {

  private Long id;
  private String fullName;
  private String emailAddress;
  private BigDecimal profileCompleteness;
}
