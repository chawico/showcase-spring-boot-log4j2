package com.chawico.showcase.bootlog4j2.dataprovider.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 * Representation of a table that describes a Person.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 12/04/2019
 */
@Getter
@Setter
@Entity
@Table(name = "tb_person")
public class PersonEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", nullable = false)
  private Long id;
  
  @Column(name = "first_name", length = 50, nullable = false)
  private String firstName;
  
  @Column(name = "last_name", length = 70, nullable = false)
  private String lastName;
  
  @Column(name = "email_address", length = 100)
  private String emailAddress;
}
