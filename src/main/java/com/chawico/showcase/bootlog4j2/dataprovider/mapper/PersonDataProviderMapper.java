package com.chawico.showcase.bootlog4j2.dataprovider.mapper;

import java.util.Optional;
import com.chawico.showcase.bootlog4j2.dataprovider.entity.PersonEntity;
import com.chawico.showcase.bootlog4j2.usecase.domain.PersonDomain;

/**
 * Converts an object retrieved from database to a domain object.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 29/04/2019
 */
public class PersonDataProviderMapper {

  /**
   * Private constructor in order to avoid instantiation.
   */
  private PersonDataProviderMapper() {

  }

  /**
   * Converts a {@code PersonEntity} to an object that represents the domain (core) of the application.
   * 
   * @param personEntity {@code Optional<PersonEntity>} - a Person retrieved from the database.
   * @return a Person according to the Domain needs. 
   */
  public static final Optional<PersonDomain> toDomain(Optional<PersonEntity> personEntity) {
    
    PersonDomain personDomain = null;
    PersonEntity entity = null;
    
    if (personEntity.isPresent()) {
      entity = personEntity.get();
      
      String fullName = entity.getFirstName()
          .concat(" ")
          .concat(entity.getLastName());
      
      personDomain = PersonDomain.builder()
          .id(entity.getId())
          .fullName(fullName)
          .emailAddress(entity.getEmailAddress())
          .build();
    }
    
    return Optional.ofNullable(personDomain);
  }
}
