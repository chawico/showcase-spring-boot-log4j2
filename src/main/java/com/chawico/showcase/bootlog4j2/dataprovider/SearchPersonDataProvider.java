package com.chawico.showcase.bootlog4j2.dataprovider;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.chawico.showcase.bootlog4j2.dataprovider.entity.PersonEntity;
import com.chawico.showcase.bootlog4j2.dataprovider.mapper.PersonDataProviderMapper;
import com.chawico.showcase.bootlog4j2.dataprovider.repository.PersonRepository;
import com.chawico.showcase.bootlog4j2.usecase.domain.PersonDomain;
import com.chawico.showcase.bootlog4j2.usecase.gateway.SearchPersonGateway;
import lombok.extern.slf4j.XSlf4j;

/**
 * Searches for a person in the database.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 29/04/2019
 */
@Service
@XSlf4j
public class SearchPersonDataProvider implements SearchPersonGateway {

  @Autowired
  private PersonRepository personRepository;
  
  @Override
  public Optional<PersonDomain> findPersonById(Long personId) {
    log.entry(personId);
    
    Optional<PersonEntity> person = this.personRepository.findById(personId);
    Optional<PersonDomain> personDomain = PersonDataProviderMapper.toDomain(person);
    
    log.exit(personDomain);
    return personDomain;
  }
}
