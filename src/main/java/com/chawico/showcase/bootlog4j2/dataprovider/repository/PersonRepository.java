package com.chawico.showcase.bootlog4j2.dataprovider.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.chawico.showcase.bootlog4j2.dataprovider.entity.PersonEntity;

/**
 * Repository that retrieves a Person from the database.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 12/04/2019
 */
@Repository
public interface PersonRepository extends CrudRepository<PersonEntity, Long> {

}
