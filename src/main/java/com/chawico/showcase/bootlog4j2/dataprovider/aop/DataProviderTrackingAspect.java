package com.chawico.showcase.bootlog4j2.dataprovider.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StopWatch;
import lombok.extern.slf4j.Slf4j;

/**
 * AOP implementation to track all outgoing transactions.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 02/05/2019
 */
@Aspect
@Configuration
@Slf4j
public class DataProviderTrackingAspect {
  
  /**
   * Tracks the time that a DataProvider public method takes to be executed
   * 
   * @param joinPoint {@code ProceedingJoinPoint} - intercepted call
   * @return the result of the method execution
   * @throws Throwable when the execution of the intercepted method fails
   */
  @Around("execution(public * com.chawico.showcase.bootlog4j2.dataprovider.*.*(..))")
  public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
    
    StopWatch stopWatch = new StopWatch();
    stopWatch.start("dataprovider");

    Object result = joinPoint.proceed(joinPoint.getArgs());
    stopWatch.stop();
    
    log.debug("Method {} took {}ms to be executed", joinPoint.getSignature(), stopWatch.getTotalTimeMillis());
    return result;
  }
}
