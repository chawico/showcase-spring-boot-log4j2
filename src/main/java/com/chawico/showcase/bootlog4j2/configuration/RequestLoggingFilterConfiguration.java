package com.chawico.showcase.bootlog4j2.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

/**
 * Configuration for logging requests.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 12/04/2019
 */
@Configuration
public class RequestLoggingFilterConfiguration {

  @Bean
  public CommonsRequestLoggingFilter requestLog() {
    CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
    filter.setIncludeQueryString(true);
    filter.setIncludePayload(true);
    filter.setMaxPayloadLength(10000);
    filter.setIncludeHeaders(true);
    filter.setAfterMessagePrefix("REQUEST DATA: ");
    return filter;
  }
}
