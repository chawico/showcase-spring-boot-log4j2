package com.chawico.showcase.bootlog4j2;

import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Responsible for the application initialization.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 12/04/2019
 */
@SpringBootApplication
public class BootLog4j2Application {
  
  private static final XLogger LOGGER = XLoggerFactory.getXLogger(BootLog4j2Application.class.getName());
  
  /**
   * Main method
   * 
   * @param args {@code String[]} - initialization arguments.
   */
  public static void main(String... args) {
    LOGGER.entry(((Object[]) args));
    SpringApplication.run(BootLog4j2Application.class, args);
  }
}
