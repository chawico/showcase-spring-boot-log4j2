package com.chawico.showcase.bootlog4j2.entrypoint.model.mapper;

import java.math.BigDecimal;
import java.util.Optional;
import com.chawico.showcase.bootlog4j2.entrypoint.model.response.PersonModelResponse;
import com.chawico.showcase.bootlog4j2.usecase.domain.PersonDomain;

/**
 * [Descrever brevemente o propósito da classe]
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 30/04/2019
 */
public class PersonEntryPointModelMapper {

  private static BigDecimal HUNDRED = new BigDecimal(100);
  
  /**
   * Private constructor to avoid instantiation.
   */
  private PersonEntryPointModelMapper() {
    
  }
  
  /**
   * Converts a {@code PersonDomain} to an object that'll be returned as a response to the request.
   * 
   * @param personDomain {@code Optional<PersonDomain>} - a Person.
   * @return a Person representation according to the output of the request.
   */
  public static final Optional<PersonModelResponse> fromDomain(Optional<PersonDomain> personDomain) {
    
    PersonModelResponse personModelResponse = null;
    
    if (personDomain.isPresent()) {
      PersonDomain person = personDomain.get();
      
      BigDecimal profileCompleteness = person.getProfileCompleteness().multiply(HUNDRED);
      String profileCompletenessPercentage = String.format("%.2f %%", profileCompleteness);
      
      personModelResponse = PersonModelResponse.builder()
          .id(person.getId())
          .fullName(person.getFullName())
          .emailAddress(person.getEmailAddress())
          .profileCompletenessPercentage(profileCompletenessPercentage)
          .build();
    }
    
    return Optional.ofNullable(personModelResponse);
  }
}
