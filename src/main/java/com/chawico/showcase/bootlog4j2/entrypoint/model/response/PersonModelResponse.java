package com.chawico.showcase.bootlog4j2.entrypoint.model.response;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Structure that represents a Person that'll be exposed to the outerworld.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 30/04/2019
 */
@Getter
@Builder
@ToString
public class PersonModelResponse {

  private Long id;
  private String fullName;
  private String emailAddress;
  private String profileCompletenessPercentage; 
}
