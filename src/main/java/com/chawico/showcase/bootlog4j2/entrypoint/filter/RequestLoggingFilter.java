package com.chawico.showcase.bootlog4j2.entrypoint.filter;

import java.io.IOException;
import java.util.function.Supplier;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.slf4j.event.Level;
import org.springframework.core.annotation.Order;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import kr.pe.kwonnam.slf4jlambda.LambdaLogger;
import kr.pe.kwonnam.slf4jlambda.LambdaLoggerFactory;

/**
 * Filters all requests in order to make some logging.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 30/04/2019
 */
@Component
@Order(1)
public class RequestLoggingFilter extends OncePerRequestFilter {

  /**
   * Logger
   */
  private static final LambdaLogger LOG = LambdaLoggerFactory.getLogger(RequestLoggingFilter.class);
  
  /**
   * Adds to MDC the received Correlation Id.
   * 
   * @param request {@code HttpServletRequest} - the request
   */
  private void processMDCCorrelationId(HttpServletRequest request) {
    String correlationID = request.getHeader("x-correlationId");
    MDC.put("correlation.id", correlationID);
  }

  /**
   * Adds to MDC the received Flow Id.
   * 
   * @param request {@code HttpServletRequest} - the request
   */
  private void processMDCFlowId(HttpServletRequest request) {
    String flowID = request.getHeader("x-flowId");
    MDC.put("flow.id", flowID);
  }

  /**
   * Creates a message with common information of a request.
   * 
   * @param request {@code HttpServletRequest} - the request
   * @return a message with common information of a request
   */
  private String createBasicRequestMessage(HttpServletRequest request) {
    StringBuilder message = new StringBuilder();
    message.append(request.getMethod());
    message.append(" uri=").append(request.getRequestURI());

    String queryString = request.getQueryString();
    if (queryString != null) {
      message.append('?').append(queryString);
    }
    
    return message.toString();
  }

  /**
   * Creates a message that represents an incoming request.
   * 
   * @param request {@code HttpServletRequest} - the request
   * @return a message that represents an incoming request
   */
  private String createRequestMessage(HttpServletRequest request) {
    StringBuilder message = new StringBuilder();
    message.append(this.createBasicRequestMessage(request));
    message.append(";headers=").append(new ServletServerHttpRequest(request).getHeaders());
    return message.toString();
  }
  
  /**
   * Creates a message that represents the response of a request.
   * 
   * @param request {@code HttpServletRequest} - the request
   * @param response {@code HttpServletResponse} - the response
   * @return a message that represents the response of a request
   */
  private String createResponseMessage(HttpServletRequest request, HttpServletResponse response, Long duration) {
    StringBuilder message = new StringBuilder();
    message.append(this.createBasicRequestMessage(request));
    message.append(" completed with STATUS ").append(response.getStatus());
    message.append(" in ").append(duration).append("ms");

    return message.toString();
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain filterChain) throws ServletException, IOException {

    processMDCCorrelationId(request);
    processMDCFlowId(request);

    long startTime = System.currentTimeMillis();
    LOG.debug("{}", () -> createRequestMessage(request));

    try {
      filterChain.doFilter(request, response);
    } finally {
      long endTime = System.currentTimeMillis();
   
      //Checking if the request succeeded
      Level level = Level.TRACE;
      String httpStatus = String.valueOf(response.getStatus());
      if (!httpStatus.startsWith("2")) {
        level = Level.ERROR;
      }
      
      Marker marker = MarkerFactory.getMarker(httpStatus);
      Supplier<?> messageSupplier = () -> createResponseMessage(request, response, endTime - startTime);
      LOG.doLog(marker, level, "{}", new Supplier<?>[]{messageSupplier}, null);

      MDC.clear();
    }
  }
}