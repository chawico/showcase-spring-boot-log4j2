package com.chawico.showcase.bootlog4j2.entrypoint;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.chawico.showcase.bootlog4j2.entrypoint.model.mapper.PersonEntryPointModelMapper;
import com.chawico.showcase.bootlog4j2.entrypoint.model.response.PersonModelResponse;
import com.chawico.showcase.bootlog4j2.usecase.SearchPersonUseCase;
import com.chawico.showcase.bootlog4j2.usecase.domain.PersonDomain;
import lombok.extern.slf4j.XSlf4j;

/**
 * Controller responsible for retrieving a Person
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 12/04/2019
 */
@RestController
@XSlf4j
public class SearchPersonController {

  @Autowired
  private SearchPersonUseCase searchPersonUseCase;

  /**
   * Searches for a person according to its identifier.
   * 
   * @param personId {@code Long} - Person identifier.
   * @return data representation of a Person
   */
  @GetMapping(value = "/person/{person_id}")
  public ResponseEntity<PersonModelResponse> searchPersonById(
      @PathVariable(name = "person_id") Long personId) {

    log.entry(personId);
    
    try {
      Optional<PersonDomain> personDomain = this.searchPersonUseCase.findPersonById(personId);
      Optional<PersonModelResponse> personResponse =
          PersonEntryPointModelMapper.fromDomain(personDomain);
      
      ResponseEntity<PersonModelResponse> result = ResponseEntity.of(personResponse);
      log.exit(result);
      return result;
    } catch (IllegalArgumentException exception) {
      ResponseStatusException responseStatusException = new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, exception.getMessage(), exception);
      
      log.throwing(responseStatusException);
      throw responseStatusException;
    }
  }
}
